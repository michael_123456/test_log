var fs = require('fs');
var moment =require('moment');
var async = require('async');
var folder;
var fileName;
var path;
var timestamp= moment().format('(MM-DD-HH_mm)');
var file_OK_flag=false;

test_log=function(folder_name){

  folder_test = '../test_log';
  folder_file=folder_test+'/'+folder_name;
  path = folder_file+'/'+timestamp+'.txt';

  async.waterfall([
    function(next){
      fs.exists(folder_test, function (exists) {
        if(exists){
          console.log('test_log exists');
          next();
        }else{
          fs.mkdir(folder_test,function(err){
            if(err){
              console.log('test_log_mkdir_Error:',err);
              throw err;
            }else{
              next();
            }
          });
        }
      });
    },

    function(next){
      fs.exists(folder_file, function (exists) {
        if(exists){
            console.log(folder_name+' exists');
          next();
        }else{
          fs.mkdir(folder_file,function(err){
            if(err){
              console.log('test_log_mkdir_Error:',err);
              throw err;
            }else{
              next();
            }
          });
        }
      });
    },

    function(next){
      console.log('here ',path);
      fs.writeFile(path,'logs \n',function(err){
        if(err){
            console.log('test_log_writeFile_Error:',err);
            throw err;
        }else{
          console.log(path+' writeFile');
          file_OK_flag=true;
          return null;
        }
      });
    },
  ]);
};
module.exports=test_log;

test_log.prototype.log = function(logs,object){
  var objects='';
  if(object){
    objects=JSON.stringify(object);
  }
  var append = '\n' + logs+objects;

  fs.appendFile(path , append, function (err) {
    if (err){
      console.log('test_log_appendFile_Error:',err);
      throw err;
    }else{
      console.log(append);
      return null;
    }
  });
};
