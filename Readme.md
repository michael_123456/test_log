# test_log #
用於將log寫入到額外的檔案中。


# 使用方式 #
1.建立物件


```
#!javascript

var test_log = require('<test_log路徑>');

var spin_log=new test_log('<資料夾名稱>');
```


2.寫入資料

```
#!javascript

spin_log.log('<要寫入的字串資料>',<要寫入的物件資料>);
```